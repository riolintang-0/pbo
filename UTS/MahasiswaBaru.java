public class MahasiswaBaru extends Mahasiswa {

    String asalSekolah;
    
    //constructor
    public MahasiswaBaru (String nim,String nama,int semeseter,int usia,String[] krs,String asalSekolah)
    {
        super(nim, nama, semeseter, usia, krs);
        this.asalSekolah = asalSekolah;
    }

    boolean ikutOspek(boolean ikut)
    {
        if(ikut == false)
        {
            return false;
        }
        return true;
    }

    
    @Override
    public void infoMahasiswa()
    {
        super.infoMahasiswa();
        System.out.println("ASAL SEKOLAH : " + asalSekolah);
    }

    
}

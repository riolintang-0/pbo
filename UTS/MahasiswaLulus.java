public class MahasiswaLulus extends Mahasiswa{

    int tahunWisuda;
    float ipk;


    //constructor milik MahasiswaLulus
    public MahasiswaLulus(String nim,String nama,int semeseter,int usia,String[] krs,int tahunWisuda,float ipk)
    {
        super(nim, nama, semeseter, usia, krs);
 
        this.tahunWisuda = tahunWisuda;
        this.ipk = ipk;
        
    }
    boolean ikutWisuda(boolean n)
    {
        if(n == false)
        {

            return false;
        }
        return true;
    }

    //@override , menimpa
    public void infoMahasiswa()
    {
        super.infoMahasiswa();
        System.out.println("TAHUN WISUDA : " + tahunWisuda);
        System.out.println("IPK : " + ipk);
    }

    
}

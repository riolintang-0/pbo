import Transportasi.Bicycle;
import Transportasi.Mobil;

public class TransportasiDemo {
    public static void main(String[] args) throws Exception {

        Bicycle b = new Bicycle();
        b.rem();

        Mobil m = new Mobil();
        m.gas();
        
    }
}

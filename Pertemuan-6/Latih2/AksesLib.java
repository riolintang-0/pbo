import id.ac.dinus.lib.MyLib; // langsung menuju class yang dituju
import id.ac.dinus.lib.YourLib;
import id.ac.dinus.test.HisLib;

public class AksesLib {
    
    public static void main(String[] args) {
       
        MyLib m = new MyLib(); //membuat objek dari class MyLib
        m.cetak(); //memanggil method cetak() dari class MyLib

        YourLib y = new YourLib(); //membuat objek dari Class YourLib
        y.cetak2(); //memanggil method cetak2() dari class YourLib

        HisLib h = new HisLib(); //membuat objek dari Class HisLib
        h.cetak3(); //memanggil method cetak3() dari class HisLib
    }
    
}

public class AksiLampu {
    public static void main(String[] args)  {

    // jika lampu menyala
    // membuat objek lampu hidup
        Lampu lampuhidup = new Lampu();
        
    //mengakses atribut dan method
        lampuhidup.hiduplampu();

        System.out.println("");
        System.out.println("");

    // jika lampu mati
    // membuat objek lampu mati
        Lampu lampumati = new Lampu();
    
    //mengakses atribut dan method
        lampumati.matilampu();

    }
}

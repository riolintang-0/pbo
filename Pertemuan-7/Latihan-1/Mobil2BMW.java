//sub class
public class Mobil2BMW extends Mobil2{

    public void nontonTV()
    {
        System.out.println("TV Dihidupkan . . .");
        System.out.println("TV Mencari Channel . . .");
        System.out.println("TV Menampilkan Gambar . . .");
    }

    public void cetak()
    {
        //super untuk memanggil parent class/ super class 
        super.hidupkanMobil();
        nontonTV();
        super.ubahGigi();
        super.matikanMobil();
    }

    
    
}

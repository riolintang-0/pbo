public class KaryawanDemo {

    public static void main(String[] args) {
        
        KaryawanTetap karyawanTetap = new KaryawanTetap("Surya", 1000000, 5000000);
        KaryawanKontrak karyawanKontrak = new KaryawanKontrak("Tono", 1000000, 30, 150000);

        karyawanKontrak.cetak();
        karyawanTetap.cetak();

    }
    
}

public class KaryawanTetap extends Karyawan{

    int gaji_pokok;

    public KaryawanTetap(String nama,int tunjangan_anak,int gaji_pokok)
    {
        super(nama, tunjangan_anak);
        this.gaji_pokok = gaji_pokok ;
    }

    public void hitung()
    {
        int total = tunjangan_anak + gaji_pokok ;
        System.out.println(total);
    }

    public void cetak()
    {
        super.cetak();
        System.out.println("Tunjangan Anak : " + tunjangan_anak);
        System.out.println("Gaji Pokok : " + gaji_pokok);
        System.out.print("Total Gaji : " );
        hitung();
    }
    
}

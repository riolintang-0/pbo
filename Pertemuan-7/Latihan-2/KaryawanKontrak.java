public class KaryawanKontrak extends Karyawan{

    int hari;
    int upah;

    public KaryawanKontrak(String nama,int tunjangan_anak,int hari,int upah)
    {
        super(nama, tunjangan_anak);
        this.hari = hari;
        this.upah = upah;
    } 

    void hitung()
    {
        int totalupah = upah*hari;
        int total = totalupah + tunjangan_anak;

        System.out.println(total);
    }

    public void cetak()
    {
        super.cetak();
        System.out.println("Upah Harian : " + upah);
        System.out.println("Total Hari : " + hari);
        System.err.print("Total Gaji : ");
        hitung();
    }
    
}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.volume_tabung;

/**
 *
 * @author Asus
 */
public class Volume_Tabung {

     public static void main(String[] args) {
 
        int diameter = 5;
        float jari = (float) diameter/2;
        int tinggi = 10;
        float phi = (float) 3.14;
        
        float volume = jari * jari * tinggi * phi;
        System.out.println("====PROGRAM MENGHITUNG VOLUME TABUNG====");
        System.out.println("Diketahui diameter = "+diameter+ " ,tinggi = "+tinggi);
        System.out.println("Volume Tabung adalah "+ String.format("%.2f", volume));
    }
}

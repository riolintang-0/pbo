/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.luas_segitiga;

/**
 *
 * @author Asus
 */
public class Luas_Segitiga {

     public static void main(String[] args) {
 
        int alas = 6;
        int tinggi = 8;
        int luas = alas*tinggi/2;
        System.out.println("====PROGRAM MENGHITUNG LUAS SEGITIGA====");
        System.out.println("Diketahui alas = "+alas+ " ,tinggi = "+tinggi);
        System.out.println("Luas Segitiga adalah "+ luas);
    }
}

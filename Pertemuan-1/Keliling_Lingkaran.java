/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.keliling_lingkaran;

/**
 *
 * @author Asus
 */
public class Keliling_Lingkaran {

    public static void main(String[] args) {
        float pi = (float) 3.14;
        int d = 10;
        float keliling = pi*d;
        System.out.println("====PROGRAM MENGHITUNG KELILING LINGKARAN====");
        System.out.println("Diketahui PI = "+pi+ " ,Diameter = "+d);
        System.out.println("Keliling Lingkaran dari diameter "+d+" adalah "+ String.format("%.2f", keliling));
    }
}

package implementasi.phone;


public class CellphoneMain {
    public static void main(String[] args){
        Cellphone cp = new Cellphone("Nokia", "3310");

        cp.powerOn();
        
        cp.tambahContact("Tono", "081231846896");
        cp.tambahContact("Tika", "085429230424");
        cp.tambahContact("Taka", "086446464647");

        cp.listContact();
        cp.cariContact("Tika");
        cp.cariContact("Budi");
        
        
      
        cp.infoVolume();
        cp.volumeUp();
        cp.volumeUp();
        cp.volumeDown();
        cp.infoVolume();
        cp.infoBattery();
     
        cp.infoPulsa();
        cp.topupPulsa(250000);
        cp.infoPulsa();

        cp.powerOff();
        cp.cariContact("Tika");
        cp.infoBattery();
    
        
    }
}

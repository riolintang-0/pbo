package implementasi.phone;
import java.lang.Math;
import java.util.ArrayList;

public class Cellphone implements Phone{
    String merk;
    String type;
    int batteryLevel;
    int status;
    int volume ;
    int topup;
    int pulsa;
    ArrayList<Contact> Kontak = new ArrayList<Contact>();

    public Cellphone(String merk,String type)
    {
        this.merk = merk;
        this.type = type;
        this.batteryLevel = (int)(Math.random()*(100-0+1)+0);  
    }

    public void powerOn(){
        this.status = 1;
        System.out.println("Ponsel menyala");
        this.volume = (int)(50);
        this.pulsa = (int)(15000);
    }
    
    public void powerOff(){
        this.status = 0;
        System.out.println("Ponsel mati");
    }

    public void volumeUp(){
        if(this.status == 0){
            System.out.println("Ponsel mati. Tidak dapat menaikkan volume");
        }
        else
        {
            this.volume++;
            if(this.volume>=MAX_VOLUME)
            {
                this.volume = MAX_VOLUME;
            }
        }
    }

   
    public void volumeDown(){
        
        if(this.status == 0){
            System.out.println("Ponsel mati. Tidak dapat menurunkan volume");
        }
        else
        {
            this.volume--;
            if(this.volume<=MIN_VOLUME)
            {
                this.volume = MIN_VOLUME;
            }
        }
        
    }

    public void infoVolume()
    {
        if(this.status == 0)
        {
            System.out.println("Ponsel mati. Tidak dapat melihat info volume");
        }else{
            
                System.out.println("Volume Sekarang " +this.volume);
            
        }
        
    }

    public void infoBattery()
    {
        if(this.status == 0)
        {
            System.out.println("Ponsel mati. Tidak dapat melihat info Baterai");
        }else{
            if(this.batteryLevel == MAX_BATT_LEVEL)
            {
                System.out.println("Baterai Sekarang Full ");
            }
            else{
                System.out.println("Baterai Sekarang " +this.batteryLevel);
            }
        }
        
    }

    public void topupPulsa(int topup)
    {
        if(this.status == 0)
        {
            System.out.println("Ponsel mati. Tidak dapat melakukan top up pulsa");
        }else{
            this.topup = topup;
            System.out.println("Melakukan Top Up Pulsa Senilai : " + this.topup);
        }
    }

    public void infoPulsa()

    {
        if(this.status == 0)
        {
            System.out.println("Ponsel mati. Tidak dapat melihat informasi pulsa");
        }else{
            this.pulsa+=this.topup;
            System.out.println("Pulsa Anda Sebesar " + this.pulsa);
        }
        
    }

    void listContact()
    {
        if(this.status == 0)
        {
            System.out.println("Ponsel Mati. Tidak dapat menjalankan perintah melihat kontak");
        }else{
            for(Contact obj : Kontak){
                System.out.println(obj.getNama()+" "+obj.getNomor());
            }
        }
    }

    void tambahContact(String nama,String nomor)
    {
        if(this.status == 0)
        {
            System.out.println("Ponsel Mati. Tidak dapat menjalankan perintah tambah kontak");
        }else{
            Kontak.add(new Contact(nama,nomor));
        }
         
    }

    void cariContact(String cari)
    {
        if(this.status == 0)
        {
            System.out.println("Ponsel Mati. Tidak dapat menjalankan perintah cari kontak");
        }else{
            int cek = 0;
        for(Contact obj : Kontak){
            if(obj.getNama()==cari){
                System.out.println(obj.getNama()+" "+obj.getNomor());
                cek = 1;
            }
            
        }
        if(cek == 0)
            {
                System.out.println("Kontak Tidak Ditemukan");
            }
        }
        
    }



}




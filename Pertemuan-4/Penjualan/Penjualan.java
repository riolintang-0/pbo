import java.util.Scanner;

public class Penjualan {

    public static void main(String[] args) throws Exception {
        boolean cek = true;
        do{
            Scanner input = new Scanner(System.in);
       
        System.out.print("Masukan Kode Barang: ");
        String kode = input.nextLine();
        System.out.print("Masukan Nama Barang: ");
        String nama = input.nextLine();
        System.out.print("Masukan Harga Barang: ");
        float harga = input.nextFloat();
        System.out.print("Masukan Jumlah Barang: ");
        int jumlah = input.nextInt();

        Jual jual1 = new Jual();

        float totHarga = jual1.getTotalBarang(harga, jumlah);

        System.out.println("Total Pembelian: " + totHarga);

        String bonus = jual1.getBonus(totHarga, jumlah);
        System.out.print("Bonus Anda: " + bonus);

        System.out.println("\n \n \n \n \n ");

        jual1.cetakNota(kode, nama, harga, jumlah, totHarga, bonus);

        System.out.println("\n \n ");

        System.out.print("Apakah ingin input lagi? [Y/T]");
        char lagi = input.next().charAt(0);
        
            if (lagi=='T')
            {
                cek = false;
            }

        }
        while(cek);
        
        
    }
}

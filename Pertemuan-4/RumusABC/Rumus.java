public class Rumus {
    
    int a;
    int b;
    int c;
    double deter;
    double akar1;
    double akar2;

    double getDeterminan(int a,int b,int c)
    {

        deter = b*b - (4*a*c);
        return deter;

    }

    double getAkar1(double deter,int a,int b,int c)
    {

        if(deter > 0)
        {
            akar1 = (-1*b + Math.sqrt(deter) ) / (2*a);
        }
        else if (deter < 0)
        {
            akar1 = (-1*b + Math.sqrt(-1*deter))/(2*a);
        }
        else if (deter == 0)
        {
            akar1 = -1*b/(2*a);
        }
        return akar1;

    }
    double getAkar2(double deter,int a,int b,int c)
    {

        
        if(deter > 0)
        {
            akar2 = (-1*b - Math.sqrt(deter) ) / (2*a);
        }
        else if (deter < 0)
        {
            akar2 = (-1*b/(2*a) - Math.sqrt(-1*deter))/ (2*a);
        }
        else if (deter == 0)
        {
            akar2 = -1*b/(2*a);
        }
        return akar2;

    }
}
